create table moto_Category (
	uuid_ VARCHAR(75) null,
	categoryId LONG not null primary key,
	categoryName VARCHAR(75) null
);

create table moto_Industry (
	uuid_ VARCHAR(75) null,
	industryId LONG not null primary key,
	industryName VARCHAR(75) null
);

create table moto_Khaliq (
	uuid_ VARCHAR(75) null,
	khaliqId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	name VARCHAR(75) null,
	designation VARCHAR(75) null,
	dob DATE null
);

create table moto_KhaliqCategoryMapping (
	uuid_ VARCHAR(75) null,
	khaliqCategoryMappingId LONG not null primary key,
	khaliqId LONG,
	categoryId LONG
);

create table moto_KhaliqIndustryMapping (
	uuid_ VARCHAR(75) null,
	khaliqIndustryMappingId LONG not null primary key,
	khaliqId LONG,
	industryId LONG
);