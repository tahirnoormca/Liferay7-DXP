package com.tahir.custworkflow;

import com.tahir.customworkflowbe.model.Feedback;
import com.tahir.customworkflowbe.service.FeedbackLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import com.liferay.asset.kernel.model.AssetRenderer;
import com.liferay.asset.kernel.model.AssetRendererFactory;
import com.liferay.asset.kernel.model.BaseAssetRendererFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;






@Component(
	   immediate = true ,
	   property = {"javax.portlet.name=_SpringMV.portlet_SpringMVPortlet.java"},
	   service = AssetRendererFactory.class
)
public class FeedbackAssetRendererFactory extends BaseAssetRendererFactory{

	@SuppressWarnings("unchecked")
	@Override
	public AssetRenderer getAssetRenderer(long classPK, int type)
	throws PortalException, SystemException {
		System.out.println("FeedbackAssetRendererFactory class is calling");
	Feedback feedback = FeedbackLocalServiceUtil.getFeedback(classPK);
	return new FeedbackAssetRenderer(feedback);
	}

	@Override
	public String getClassName() {
	return CLASS_NAME;
	}
	@Override
	public String getType() {
	return TYPE;
	}
	public static final String TYPE = "Feedback";
	public static final String CLASS_NAME = Feedback.class.getName();


}
