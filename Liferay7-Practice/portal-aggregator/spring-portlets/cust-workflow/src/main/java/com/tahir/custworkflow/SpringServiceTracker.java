package com.tahir.custworkflow;


import com.tahir.customworkflowbe.service.FeedbackLocalService;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;


public class SpringServiceTracker extends ServiceTracker<FeedbackLocalService, FeedbackLocalService> {
    public SpringServiceTracker(Object host) {
        super(FrameworkUtil.getBundle(host.getClass()).getBundleContext(), FeedbackLocalService.class, null);

    }


}