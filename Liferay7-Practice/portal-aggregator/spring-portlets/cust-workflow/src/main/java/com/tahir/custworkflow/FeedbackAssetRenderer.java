package com.tahir.custworkflow;

import java.util.Locale;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.liferay.asset.kernel.model.BaseAssetRenderer;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;


import com.tahir.customworkflowbe.model.Feedback;


public class FeedbackAssetRenderer extends BaseAssetRenderer{
	

private Feedback _feedback;

public FeedbackAssetRenderer(Feedback feedback) {
	System.out.println("feedback assetrender method is calling");
_feedback = feedback;
}

public long getClassPK() {
return _feedback.getFeedbackId();
}

public long getGroupId() {
return _feedback.getGroupId();
}

public String getSummary(Locale arg0) {
return _feedback.getFeedbackText();
}

public String getTitle(Locale arg0) {
return _feedback.getFeedbackSubject();
}

public long getUserId() {
return _feedback.getUserId();
}

public String getUuid() {
return _feedback.getUuid();
}

public String render(RenderRequest request, RenderResponse response, String template)
throws Exception
{
	System.out.println("AssetRenderer method ");
if (template.equals(TEMPLATE_FULL_CONTENT)) {
request.setAttribute("feedBackObject",_feedback);
return "/webapp/WEB-INF/jsp/view_feedbck.jsp";
}
else
{
return null;
}
}

@Override
public String getUserName() {
// TODO Auto-generated method stub
String userName=null;
try {
userName= UserLocalServiceUtil.getUser(_feedback.getUserId()).getFullName();
} catch (PortalException e) {
// TODO Auto-generated catch block
e.printStackTrace();
} catch (SystemException e) {
// TODO Auto-generated catch block
e.printStackTrace();
return userName;
}
return userName;
}

@Override
public String getClassName() {
// TODO Auto-generated method stub
return Feedback.class.getName();
}

@Override
public Object getAssetObject() {
	// TODO Auto-generated method stub
	return null;
}


@Override
public String getSummary(PortletRequest portletRequest, PortletResponse portletResponse) {
	// TODO Auto-generated method stub
	return null;
}

@Override
public boolean include(HttpServletRequest request, HttpServletResponse response, String template) throws Exception {
	// TODO Auto-generated method stub
	return false;
}

}
