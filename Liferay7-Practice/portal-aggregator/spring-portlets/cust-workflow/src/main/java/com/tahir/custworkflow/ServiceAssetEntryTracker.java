package com.tahir.custworkflow;

import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

import com.liferay.asset.kernel.service.AssetEntryLocalService;

public class ServiceAssetEntryTracker extends ServiceTracker<AssetEntryLocalService, AssetEntryLocalService>{

	public ServiceAssetEntryTracker(Object host)
	{
		
		super(FrameworkUtil.getBundle(host.getClass()).getBundleContext(),AssetEntryLocalService.class,null);
	}
}
