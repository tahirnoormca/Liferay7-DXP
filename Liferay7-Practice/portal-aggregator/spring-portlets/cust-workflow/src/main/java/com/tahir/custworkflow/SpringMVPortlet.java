package com.tahir.custworkflow;

import com.liferay.asset.kernel.service.AssetEntryLocalServiceUtil;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.NoSuchWorkflowDefinitionLinkException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.WorkflowDefinitionLink;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.service.WorkflowDefinitionLinkLocalServiceUtil;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowHandlerRegistryUtil;



import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import com.tahir.customworkflowbe.model.Feedback;
import com.tahir.customworkflowbe.service.FeedbackLocalService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * @author Pro
 */

@Controller(value = "SpringMVPortlet")
@RequestMapping("VIEW")
public class SpringMVPortlet{
	public static SpringServiceTracker springServiceTracker = null;
	public static ServiceClaimAgent serviceClaimAgent=null;
	public static ServiceWorkflowTracker  serviceWorkflowTracker=null;
	public static ServiceAssetEntryTracker serviceAssetEntryTracker=null;

	@PostConstruct
	public void init() {
		System.out.println("Inside the init Method");
		
		springServiceTracker = new SpringServiceTracker(this);
		springServiceTracker.open();
		ServiceClaimAgent serviceClaimAgent=new ServiceClaimAgent(this);
		serviceClaimAgent.open();
		ServiceWorkflowTracker  serviceWorkflowTracker=new ServiceWorkflowTracker(this);
		serviceWorkflowTracker.open();
		ServiceAssetEntryTracker serviceAssetEntryTracker=new ServiceAssetEntryTracker(this);
		serviceAssetEntryTracker.open();
	}	

	@RenderMapping
	 public String view(RenderRequest request,RenderResponse response,Model model){
	  System.out.println("view>>>>>>abc>>>>>>>>>>>>>>..");
	  return "view";
	 }	

	@RenderMapping(params="render=feedback")
	public String feedback(RenderRequest rendereRequest,RenderResponse renderesponse ) throws PortalException
	{
		return "feedback";
	}
	
	//@RenderMapping(params="render=feedback")
	@ActionMapping(params="action=feedbackForm")
	public void feedback(ActionRequest request,ActionResponse response) throws PortalException
	{
		
		springServiceTracker = new SpringServiceTracker(this);
		springServiceTracker.open();
		FeedbackLocalService feedbackLocalService=springServiceTracker.getService();
		
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		String feedBackMessage = ParamUtil.getString(request, "feedbackMessage");
		String feedbackSubject = ParamUtil.getString(request, "feedbackSubject");
		com.liferay.portal.kernel.service.ServiceContext serviceContext = ServiceContextFactory.getInstance(Feedback.class.getName(), request);
		try {
			//add Feedback data
		Feedback feedback=feedbackLocalService.createFeedback(CounterLocalServiceUtil.increment());
		feedback.setCompanyId(themeDisplay.getCompanyId());
		feedback.setGroupId(themeDisplay.getScopeGroupId());
		feedback.setFeedbackDate(new Date());
		feedback.setFeedbackText(feedBackMessage);
		feedback.setFeedbackSubject(feedbackSubject);
		feedback.setFeedBackStatus(WorkflowConstants.STATUS_DRAFT);
		feedback.setUserId(themeDisplay.getUserId());
		feedback.setStatusByUserId(themeDisplay.getUserId());
		feedback=feedbackLocalService.addFeedback(feedback);
		WorkflowDefinitionLink workflowDefinitionLink=null;
		try{
		workflowDefinitionLink=
		WorkflowDefinitionLinkLocalServiceUtil.
		getDefaultWorkflowDefinitionLink(themeDisplay.getCompanyId(), Feedback.class.getName(), 0, 0);
		}catch (Exception e) {
		if(e instanceof NoSuchWorkflowDefinitionLinkException){
		SessionMessages.add(request, "workflow-not-enabled");
		}
		e.printStackTrace();
		}
		if(feedback!=null&&workflowDefinitionLink!=null){
		//add feedback asset in asset entry table
		AssetEntryLocalServiceUtil.updateEntry(themeDisplay.getUserId(), feedback.getGroupId(),
		Feedback.class.getName(), feedback.getFeedbackId(),
		serviceContext.getAssetCategoryIds(),
		serviceContext.getAssetTagNames());
		//start workflow instance to feedback.
		WorkflowHandlerRegistryUtil.startWorkflowInstance(
		feedback.getCompanyId(), feedback.getGroupId(), themeDisplay.getUserId(),
		Feedback.class.getName(), feedback.getPrimaryKey(), feedback,
		serviceContext);
		}
		
		} catch (Exception e) {
		if(e instanceof NoSuchWorkflowDefinitionLinkException){
		}
		e.printStackTrace();
		}
		response.setRenderParameter("mvcPath" , "/feedback.jsp");
		}
@PreDestroy
	
	public void destroy(){
		 springServiceTracker.close();
		 serviceClaimAgent.close();
		 serviceWorkflowTracker.close();
		 serviceAssetEntryTracker.close();
		 
	}
	
}