/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import com.tahir.customworkflowbe.exception.NoSuchSiteformException;
import com.tahir.customworkflowbe.model.Siteform;
import com.tahir.customworkflowbe.model.impl.SiteformImpl;
import com.tahir.customworkflowbe.model.impl.SiteformModelImpl;
import com.tahir.customworkflowbe.service.persistence.SiteformPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the siteform service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SiteformPersistence
 * @see com.tahir.customworkflowbe.service.persistence.SiteformUtil
 * @generated
 */
@ProviderType
public class SiteformPersistenceImpl extends BasePersistenceImpl<Siteform>
	implements SiteformPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link SiteformUtil} to access the siteform persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = SiteformImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SiteformModelImpl.ENTITY_CACHE_ENABLED,
			SiteformModelImpl.FINDER_CACHE_ENABLED, SiteformImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SiteformModelImpl.ENTITY_CACHE_ENABLED,
			SiteformModelImpl.FINDER_CACHE_ENABLED, SiteformImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SiteformModelImpl.ENTITY_CACHE_ENABLED,
			SiteformModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public SiteformPersistenceImpl() {
		setModelClass(Siteform.class);
	}

	/**
	 * Caches the siteform in the entity cache if it is enabled.
	 *
	 * @param siteform the siteform
	 */
	@Override
	public void cacheResult(Siteform siteform) {
		entityCache.putResult(SiteformModelImpl.ENTITY_CACHE_ENABLED,
			SiteformImpl.class, siteform.getPrimaryKey(), siteform);

		siteform.resetOriginalValues();
	}

	/**
	 * Caches the siteforms in the entity cache if it is enabled.
	 *
	 * @param siteforms the siteforms
	 */
	@Override
	public void cacheResult(List<Siteform> siteforms) {
		for (Siteform siteform : siteforms) {
			if (entityCache.getResult(SiteformModelImpl.ENTITY_CACHE_ENABLED,
						SiteformImpl.class, siteform.getPrimaryKey()) == null) {
				cacheResult(siteform);
			}
			else {
				siteform.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all siteforms.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(SiteformImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the siteform.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Siteform siteform) {
		entityCache.removeResult(SiteformModelImpl.ENTITY_CACHE_ENABLED,
			SiteformImpl.class, siteform.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Siteform> siteforms) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Siteform siteform : siteforms) {
			entityCache.removeResult(SiteformModelImpl.ENTITY_CACHE_ENABLED,
				SiteformImpl.class, siteform.getPrimaryKey());
		}
	}

	/**
	 * Creates a new siteform with the primary key. Does not add the siteform to the database.
	 *
	 * @param siteFormId the primary key for the new siteform
	 * @return the new siteform
	 */
	@Override
	public Siteform create(long siteFormId) {
		Siteform siteform = new SiteformImpl();

		siteform.setNew(true);
		siteform.setPrimaryKey(siteFormId);

		return siteform;
	}

	/**
	 * Removes the siteform with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param siteFormId the primary key of the siteform
	 * @return the siteform that was removed
	 * @throws NoSuchSiteformException if a siteform with the primary key could not be found
	 */
	@Override
	public Siteform remove(long siteFormId) throws NoSuchSiteformException {
		return remove((Serializable)siteFormId);
	}

	/**
	 * Removes the siteform with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the siteform
	 * @return the siteform that was removed
	 * @throws NoSuchSiteformException if a siteform with the primary key could not be found
	 */
	@Override
	public Siteform remove(Serializable primaryKey)
		throws NoSuchSiteformException {
		Session session = null;

		try {
			session = openSession();

			Siteform siteform = (Siteform)session.get(SiteformImpl.class,
					primaryKey);

			if (siteform == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchSiteformException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(siteform);
		}
		catch (NoSuchSiteformException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Siteform removeImpl(Siteform siteform) {
		siteform = toUnwrappedModel(siteform);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(siteform)) {
				siteform = (Siteform)session.get(SiteformImpl.class,
						siteform.getPrimaryKeyObj());
			}

			if (siteform != null) {
				session.delete(siteform);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (siteform != null) {
			clearCache(siteform);
		}

		return siteform;
	}

	@Override
	public Siteform updateImpl(Siteform siteform) {
		siteform = toUnwrappedModel(siteform);

		boolean isNew = siteform.isNew();

		SiteformModelImpl siteformModelImpl = (SiteformModelImpl)siteform;

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (siteform.getCreateDate() == null)) {
			if (serviceContext == null) {
				siteform.setCreateDate(now);
			}
			else {
				siteform.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!siteformModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				siteform.setModifiedDate(now);
			}
			else {
				siteform.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (siteform.isNew()) {
				session.save(siteform);

				siteform.setNew(false);
			}
			else {
				siteform = (Siteform)session.merge(siteform);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		entityCache.putResult(SiteformModelImpl.ENTITY_CACHE_ENABLED,
			SiteformImpl.class, siteform.getPrimaryKey(), siteform, false);

		siteform.resetOriginalValues();

		return siteform;
	}

	protected Siteform toUnwrappedModel(Siteform siteform) {
		if (siteform instanceof SiteformImpl) {
			return siteform;
		}

		SiteformImpl siteformImpl = new SiteformImpl();

		siteformImpl.setNew(siteform.isNew());
		siteformImpl.setPrimaryKey(siteform.getPrimaryKey());

		siteformImpl.setSiteFormId(siteform.getSiteFormId());
		siteformImpl.setSiteId(siteform.getSiteId());
		siteformImpl.setSiteName(siteform.getSiteName());
		siteformImpl.setScreen1(siteform.getScreen1());
		siteformImpl.setScreen2(siteform.getScreen2());
		siteformImpl.setScreen3(siteform.getScreen3());
		siteformImpl.setScreen4(siteform.getScreen4());
		siteformImpl.setScreen5(siteform.getScreen5());
		siteformImpl.setScreen6(siteform.getScreen6());
		siteformImpl.setScreen7(siteform.getScreen7());
		siteformImpl.setScreen8(siteform.getScreen8());
		siteformImpl.setScreen9(siteform.getScreen9());
		siteformImpl.setScreen10(siteform.getScreen10());
		siteformImpl.setCreatedBy(siteform.getCreatedBy());
		siteformImpl.setModifiedBy(siteform.getModifiedBy());
		siteformImpl.setCreateDate(siteform.getCreateDate());
		siteformImpl.setModifiedDate(siteform.getModifiedDate());

		return siteformImpl;
	}

	/**
	 * Returns the siteform with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the siteform
	 * @return the siteform
	 * @throws NoSuchSiteformException if a siteform with the primary key could not be found
	 */
	@Override
	public Siteform findByPrimaryKey(Serializable primaryKey)
		throws NoSuchSiteformException {
		Siteform siteform = fetchByPrimaryKey(primaryKey);

		if (siteform == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchSiteformException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return siteform;
	}

	/**
	 * Returns the siteform with the primary key or throws a {@link NoSuchSiteformException} if it could not be found.
	 *
	 * @param siteFormId the primary key of the siteform
	 * @return the siteform
	 * @throws NoSuchSiteformException if a siteform with the primary key could not be found
	 */
	@Override
	public Siteform findByPrimaryKey(long siteFormId)
		throws NoSuchSiteformException {
		return findByPrimaryKey((Serializable)siteFormId);
	}

	/**
	 * Returns the siteform with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the siteform
	 * @return the siteform, or <code>null</code> if a siteform with the primary key could not be found
	 */
	@Override
	public Siteform fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(SiteformModelImpl.ENTITY_CACHE_ENABLED,
				SiteformImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Siteform siteform = (Siteform)serializable;

		if (siteform == null) {
			Session session = null;

			try {
				session = openSession();

				siteform = (Siteform)session.get(SiteformImpl.class, primaryKey);

				if (siteform != null) {
					cacheResult(siteform);
				}
				else {
					entityCache.putResult(SiteformModelImpl.ENTITY_CACHE_ENABLED,
						SiteformImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(SiteformModelImpl.ENTITY_CACHE_ENABLED,
					SiteformImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return siteform;
	}

	/**
	 * Returns the siteform with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param siteFormId the primary key of the siteform
	 * @return the siteform, or <code>null</code> if a siteform with the primary key could not be found
	 */
	@Override
	public Siteform fetchByPrimaryKey(long siteFormId) {
		return fetchByPrimaryKey((Serializable)siteFormId);
	}

	@Override
	public Map<Serializable, Siteform> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Siteform> map = new HashMap<Serializable, Siteform>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Siteform siteform = fetchByPrimaryKey(primaryKey);

			if (siteform != null) {
				map.put(primaryKey, siteform);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(SiteformModelImpl.ENTITY_CACHE_ENABLED,
					SiteformImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Siteform)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_SITEFORM_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append(String.valueOf(primaryKey));

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Siteform siteform : (List<Siteform>)q.list()) {
				map.put(siteform.getPrimaryKeyObj(), siteform);

				cacheResult(siteform);

				uncachedPrimaryKeys.remove(siteform.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(SiteformModelImpl.ENTITY_CACHE_ENABLED,
					SiteformImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the siteforms.
	 *
	 * @return the siteforms
	 */
	@Override
	public List<Siteform> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the siteforms.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of siteforms
	 * @param end the upper bound of the range of siteforms (not inclusive)
	 * @return the range of siteforms
	 */
	@Override
	public List<Siteform> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the siteforms.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of siteforms
	 * @param end the upper bound of the range of siteforms (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of siteforms
	 */
	@Override
	public List<Siteform> findAll(int start, int end,
		OrderByComparator<Siteform> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the siteforms.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link SiteformModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of siteforms
	 * @param end the upper bound of the range of siteforms (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of siteforms
	 */
	@Override
	public List<Siteform> findAll(int start, int end,
		OrderByComparator<Siteform> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Siteform> list = null;

		if (retrieveFromCache) {
			list = (List<Siteform>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_SITEFORM);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_SITEFORM;

				if (pagination) {
					sql = sql.concat(SiteformModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Siteform>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Siteform>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the siteforms from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Siteform siteform : findAll()) {
			remove(siteform);
		}
	}

	/**
	 * Returns the number of siteforms.
	 *
	 * @return the number of siteforms
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_SITEFORM);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return SiteformModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the siteform persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(SiteformImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_SITEFORM = "SELECT siteform FROM Siteform siteform";
	private static final String _SQL_SELECT_SITEFORM_WHERE_PKS_IN = "SELECT siteform FROM Siteform siteform WHERE siteFormId IN (";
	private static final String _SQL_COUNT_SITEFORM = "SELECT COUNT(siteform) FROM Siteform siteform";
	private static final String _ORDER_BY_ENTITY_ALIAS = "siteform.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Siteform exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(SiteformPersistenceImpl.class);
}