/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;

import aQute.bnd.annotation.ProviderType;
import com.tahir.customworkflowbe.model.Source;
import com.tahir.customworkflowbe.service.SourceLocalServiceUtil;
import com.tahir.customworkflowbe.service.base.SourceServiceBaseImpl;


/**
 * The implementation of the source remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link data.analysis.service.SourceService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS tacredentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SourceServiceBaseImpl
 * @see data.analysis.service.SourceServiceUtil
 */
@ProviderType
public class SourceServiceImpl extends SourceServiceBaseImpl {
	
	public Source getSourceDetails(long sourceId) throws PortalException
	{
		 return	SourceLocalServiceUtil.getSource(sourceId);
	}
		
}
	/*	Source source = SourceLocalServiceUtil.getSource(sourceId);
		List<SourceBean> sourceBean=new ArrayList<SourceBean>();
		SourceBean sourceTest=new SourceBean();
		sourceTest.setGroupId((String.valueOf(source.getGroupId())));
		sourceTest.setCompanyId(String.valueOf(source.getCompanyId()));
		sourceTest.setUserName((String)source.getUserName());
		sourceTest.setColumnName((String)source.getColumnName());
		sourceTest.setCreateDate((String)source.getCreateDate().toString());
		sourceTest.setModifiedDate((String)source.getModifiedDate().toString());
		sourceTest.setStatus((String.valueOf(source.getStatus())));
		sourceTest.setTitle((String)source.getTitle());
		*/
		
		
     // return sourceTest;				
	
	
	
	
