package com.tahir.customworkflowbe.service.persistence.impl;

import java.util.List;

import com.liferay.portal.dao.orm.custom.sql.CustomSQLUtil;
import com.liferay.portal.kernel.dao.orm.PortalCustomSQLUtil;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;

import com.tahir.customworkflowbe.model.Source;
import com.tahir.customworkflowbe.model.impl.SourceImpl;
import com.tahir.customworkflowbe.service.persistence.SourceFinder;


public class SourceFinderImpl extends SourceFinderBaseImpl implements SourceFinder
{
	public static String FIND_BOOKS = "findBooks";
	public List<Source> findBooks(String name) throws SystemException {
		try
		{
		System.out.println("name ::"+name);
	    System.out.println("Before Sql ");
		String sql=CustomSQLUtil.get(getClass(),FIND_BOOKS);
		System.out.println("Sql ::"+sql);
		SQLQuery query=getCurrentSession().createSQLQuery(sql);
		System.out.println("Query :: "+query);
		query.addEntity("Source", SourceImpl.class);
		QueryPos queryPos=QueryPos.getInstance(query);
		queryPos.add(name);
		return (List<Source>)query.list();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

}
