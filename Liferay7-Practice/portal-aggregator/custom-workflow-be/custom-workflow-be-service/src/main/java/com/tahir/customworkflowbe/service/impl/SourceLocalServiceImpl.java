/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.customworkflowbe.service.impl;

import java.util.Date;
import java.util.List;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;

import aQute.bnd.annotation.ProviderType;
import com.tahir.customworkflowbe.model.Source;
import com.tahir.customworkflowbe.service.SourceLocalServiceUtil;
import com.tahir.customworkflowbe.service.base.SourceLocalServiceBaseImpl;
import com.tahir.customworkflowbe.service.persistence.SourceUtil;


/**
 * The implementation of the source local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link data.analysis.service.SourceLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see SourceLocalServiceBaseImpl
 * @see data.analysis.service.SourceLocalServiceUtil
 */
@ProviderType
public class SourceLocalServiceImpl extends SourceLocalServiceBaseImpl {
	public void add(String username,String title)
	{
		try
		{
		Source source= SourceLocalServiceUtil.createSource(CounterLocalServiceUtil.increment());
		source.setUserName(username);
		source.setTitle(title);
		source.setCreateDate(new Date());
		SourceLocalServiceUtil.addSource(source);
		
		System.out.println("Records Successfully Added..");
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
public List<Source> getAllEntries(String name)
{
	System.out.println("In Get All Entries :: "+name);
	return sourceFinder.findBooks("%" + name + "%");
	
}


public List<Source> findbyUserId(long userId)
{
	return SourceUtil.findByuserId(userId);
}
}