/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.leaveapproverdb.service.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.kernel.workflow.WorkflowHandlerRegistryUtil;
import com.tahir.leaveapproverdb.model.Leave;
import com.tahir.leaveapproverdb.service.base.LeaveLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the leave local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.tahir.leaveapproverdb.service.LeaveLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Tahir
 * @see LeaveLocalServiceBaseImpl
 * @see com.tahir.leaveapproverdb.service.LeaveLocalServiceUtil
 */
@ProviderType
public class LeaveLocalServiceImpl extends LeaveLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link com.tahir.leaveapproverdb.service.LeaveLocalServiceUtil} to access the leave local service.
	 */
	public List<Leave> getLeaveByStatus(long groupId,int status){
	    return leavePersistence.findBystatus(groupId,status);
    }
    public List<Leave> getLeaveByStatus(long groupId,int status,int start,int end){
        return leavePersistence.findBystatus(groupId,status,start,end);
    }
    public Leave updateStatus(long userId,long leaveId,int status,ServiceContext serviceContext){

        Leave leave = leavePersistence.fetchByPrimaryKey(leaveId);
        leave.setStatus(status);
        leave.setStatusByUserId(userId);
        leave.setStatusDate(new Date());
        User user = null;
        try {
            user = userLocalService.getUser(userId);
            leave.setStatusByUserName(user.getFullName());
            leave.setStatusByUserUuid(user.getUserUuid());
        } catch (PortalException e) {
            e.printStackTrace();
        }
        leave = leavePersistence.update(leave);
        try {
            if (status == WorkflowConstants.STATUS_APPROVED) {
                // update the asset status to visibile
                assetEntryLocalService.updateEntry(Leave.class.getName(), leaveId, new Date(),null, true, true);
            } else {
                // set leave entity status to false
                assetEntryLocalService.updateVisible(Leave.class.getName(), leaveId, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return leave;
    }
    public Leave addLeave(ServiceContext serviceContext, String leaveName, Date startDate,Date leaveEndDate ){

        long leaveId = counterLocalService.increment(Leave.class.getName());
        Leave leave = null;
        try {
            User user = userLocalService.getUser(serviceContext.getUserId());
            leave = leaveLocalService.createLeave(leaveId);
            leave.setUserId(serviceContext.getUserId());
            leave.setCreateDate(new Date());
            leave.setLeaveName(leaveName);
            leave.setStartDate(startDate);
            leave.setEndDate(leaveEndDate);
            leave.setUserName(user.getFullName());
            leave.setCompanyId(serviceContext.getCompanyId());
            leave.setGroupId(serviceContext.getScopeGroupId());

            leave.setStatus(WorkflowConstants.STATUS_DRAFT);
            leave.setStatusByUserId(user.getUserId());
            leave.setStatusDate(new Date());
            leave.setStatusByUserName(user.getFullName());
            leave.setStatusByUserUuid(user.getUserUuid());
            leave = leaveLocalService.addLeave(leave);
            AssetEntry assetEntry = assetEntryLocalService.updateEntry( user.getUserId(), serviceContext.getScopeGroupId(), new Date(),
                    new Date(), Leave.class.getName(),leave.getLeaveId(), leave.getUuid(), 0, null, null, true, false, new Date(), null,
                    new Date(), null, ContentTypes.TEXT_HTML, leave.getLeaveName(), leave.getLeaveName(), null, null, null, 0, 0, null);

            Indexer<Leave> indexer = IndexerRegistryUtil.nullSafeGetIndexer(Leave.class);
            indexer.reindex(leave);

            WorkflowHandlerRegistryUtil.startWorkflowInstance(leave.getCompanyId(), leave.getGroupId(), leave.getUserId(), Leave.class.getName(),
                    leave.getPrimaryKey(), leave, serviceContext);
        } catch (PortalException e) {
            e.printStackTrace();
        }
        return leave;
    }
    public int getLeaveCount(long userId){
        return  leavePersistence.countByuserId(userId);
    }
    public Leave addLeave( String leaveName,long userId,long groupId,long companyId, Date startDate,Date leaveEndDate ){

        long leaveId = counterLocalService.increment(Leave.class.getName());
        Leave leave = null;
        try {
            User user = userLocalService.getUser(userId);
            leave = leaveLocalService.createLeave(leaveId);
            leave.setUserId(userId);
            leave.setCreateDate(new Date());
            leave.setLeaveName(leaveName);
            leave.setStartDate(startDate);
            leave.setEndDate(leaveEndDate);
            leave.setUserName(user.getFullName());
            leave.setCompanyId(companyId);
            leave.setGroupId(groupId);
            leave = leaveLocalService.addLeave(leave);
        } catch (PortalException e) {
            e.printStackTrace();
        }
        return leave;
    }
}