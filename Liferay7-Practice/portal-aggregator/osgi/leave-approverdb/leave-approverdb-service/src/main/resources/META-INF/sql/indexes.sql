create index IX_55F0FE03 on WF_Leave (groupId, status);
create index IX_A8866667 on WF_Leave (userId);
create index IX_5FD7BE1 on WF_Leave (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_DAC586A3 on WF_Leave (uuid_[$COLUMN_LENGTH:75$], groupId);