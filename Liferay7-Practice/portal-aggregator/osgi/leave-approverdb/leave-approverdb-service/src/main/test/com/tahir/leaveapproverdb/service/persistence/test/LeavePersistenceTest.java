/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.tahir.leaveapproverdb.service.persistence.test;

import com.liferay.arquillian.extension.junit.bridge.junit.Arquillian;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.test.ReflectionTestUtil;
import com.liferay.portal.kernel.test.rule.AggregateTestRule;
import com.liferay.portal.kernel.test.rule.TransactionalTestRule;
import com.liferay.portal.kernel.test.util.RandomTestUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.util.IntegerWrapper;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.OrderByComparatorFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Time;
import com.liferay.portal.test.rule.LiferayIntegrationTestRule;
import com.liferay.portal.test.rule.PersistenceTestRule;

import com.tahir.leaveapproverdb.exception.NoSuchLeaveException;
import com.tahir.leaveapproverdb.model.Leave;
import com.tahir.leaveapproverdb.service.LeaveLocalServiceUtil;
import com.tahir.leaveapproverdb.service.persistence.LeavePersistence;
import com.tahir.leaveapproverdb.service.persistence.LeaveUtil;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import org.junit.runner.RunWith;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @generated
 */
@RunWith(Arquillian.class)
public class LeavePersistenceTest {
	@ClassRule
	@Rule
	public static final AggregateTestRule aggregateTestRule = new AggregateTestRule(new LiferayIntegrationTestRule(),
			PersistenceTestRule.INSTANCE,
			new TransactionalTestRule(Propagation.REQUIRED));

	@Before
	public void setUp() {
		_persistence = LeaveUtil.getPersistence();

		Class<?> clazz = _persistence.getClass();

		_dynamicQueryClassLoader = clazz.getClassLoader();
	}

	@After
	public void tearDown() throws Exception {
		Iterator<Leave> iterator = _leaves.iterator();

		while (iterator.hasNext()) {
			_persistence.remove(iterator.next());

			iterator.remove();
		}
	}

	@Test
	public void testCreate() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Leave leave = _persistence.create(pk);

		Assert.assertNotNull(leave);

		Assert.assertEquals(leave.getPrimaryKey(), pk);
	}

	@Test
	public void testRemove() throws Exception {
		Leave newLeave = addLeave();

		_persistence.remove(newLeave);

		Leave existingLeave = _persistence.fetchByPrimaryKey(newLeave.getPrimaryKey());

		Assert.assertNull(existingLeave);
	}

	@Test
	public void testUpdateNew() throws Exception {
		addLeave();
	}

	@Test
	public void testUpdateExisting() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Leave newLeave = _persistence.create(pk);

		newLeave.setUuid(RandomTestUtil.randomString());

		newLeave.setGroupId(RandomTestUtil.nextLong());

		newLeave.setCompanyId(RandomTestUtil.nextLong());

		newLeave.setUserId(RandomTestUtil.nextLong());

		newLeave.setUserName(RandomTestUtil.randomString());

		newLeave.setCreateDate(RandomTestUtil.nextDate());

		newLeave.setModifiedDate(RandomTestUtil.nextDate());

		newLeave.setLeaveName(RandomTestUtil.randomString());

		newLeave.setStartDate(RandomTestUtil.nextDate());

		newLeave.setEndDate(RandomTestUtil.nextDate());

		newLeave.setStatus(RandomTestUtil.nextInt());

		newLeave.setStatusByUserId(RandomTestUtil.nextLong());

		newLeave.setStatusByUserName(RandomTestUtil.randomString());

		newLeave.setStatusDate(RandomTestUtil.nextDate());

		_leaves.add(_persistence.update(newLeave));

		Leave existingLeave = _persistence.findByPrimaryKey(newLeave.getPrimaryKey());

		Assert.assertEquals(existingLeave.getUuid(), newLeave.getUuid());
		Assert.assertEquals(existingLeave.getLeaveId(), newLeave.getLeaveId());
		Assert.assertEquals(existingLeave.getGroupId(), newLeave.getGroupId());
		Assert.assertEquals(existingLeave.getCompanyId(),
			newLeave.getCompanyId());
		Assert.assertEquals(existingLeave.getUserId(), newLeave.getUserId());
		Assert.assertEquals(existingLeave.getUserName(), newLeave.getUserName());
		Assert.assertEquals(Time.getShortTimestamp(
				existingLeave.getCreateDate()),
			Time.getShortTimestamp(newLeave.getCreateDate()));
		Assert.assertEquals(Time.getShortTimestamp(
				existingLeave.getModifiedDate()),
			Time.getShortTimestamp(newLeave.getModifiedDate()));
		Assert.assertEquals(existingLeave.getLeaveName(),
			newLeave.getLeaveName());
		Assert.assertEquals(Time.getShortTimestamp(existingLeave.getStartDate()),
			Time.getShortTimestamp(newLeave.getStartDate()));
		Assert.assertEquals(Time.getShortTimestamp(existingLeave.getEndDate()),
			Time.getShortTimestamp(newLeave.getEndDate()));
		Assert.assertEquals(existingLeave.getStatus(), newLeave.getStatus());
		Assert.assertEquals(existingLeave.getStatusByUserId(),
			newLeave.getStatusByUserId());
		Assert.assertEquals(existingLeave.getStatusByUserName(),
			newLeave.getStatusByUserName());
		Assert.assertEquals(Time.getShortTimestamp(
				existingLeave.getStatusDate()),
			Time.getShortTimestamp(newLeave.getStatusDate()));
	}

	@Test
	public void testCountByUuid() throws Exception {
		_persistence.countByUuid(StringPool.BLANK);

		_persistence.countByUuid(StringPool.NULL);

		_persistence.countByUuid((String)null);
	}

	@Test
	public void testCountByUUID_G() throws Exception {
		_persistence.countByUUID_G(StringPool.BLANK, RandomTestUtil.nextLong());

		_persistence.countByUUID_G(StringPool.NULL, 0L);

		_persistence.countByUUID_G((String)null, 0L);
	}

	@Test
	public void testCountByUuid_C() throws Exception {
		_persistence.countByUuid_C(StringPool.BLANK, RandomTestUtil.nextLong());

		_persistence.countByUuid_C(StringPool.NULL, 0L);

		_persistence.countByUuid_C((String)null, 0L);
	}

	@Test
	public void testCountByuserId() throws Exception {
		_persistence.countByuserId(RandomTestUtil.nextLong());

		_persistence.countByuserId(0L);
	}

	@Test
	public void testCountBystatus() throws Exception {
		_persistence.countBystatus(RandomTestUtil.nextLong(),
			RandomTestUtil.nextInt());

		_persistence.countBystatus(0L, 0);
	}

	@Test
	public void testFindByPrimaryKeyExisting() throws Exception {
		Leave newLeave = addLeave();

		Leave existingLeave = _persistence.findByPrimaryKey(newLeave.getPrimaryKey());

		Assert.assertEquals(existingLeave, newLeave);
	}

	@Test(expected = NoSuchLeaveException.class)
	public void testFindByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		_persistence.findByPrimaryKey(pk);
	}

	@Test
	public void testFindAll() throws Exception {
		_persistence.findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			getOrderByComparator());
	}

	protected OrderByComparator<Leave> getOrderByComparator() {
		return OrderByComparatorFactoryUtil.create("WF_Leave", "uuid", true,
			"leaveId", true, "groupId", true, "companyId", true, "userId",
			true, "userName", true, "createDate", true, "modifiedDate", true,
			"leaveName", true, "startDate", true, "endDate", true, "status",
			true, "statusByUserId", true, "statusByUserName", true,
			"statusDate", true);
	}

	@Test
	public void testFetchByPrimaryKeyExisting() throws Exception {
		Leave newLeave = addLeave();

		Leave existingLeave = _persistence.fetchByPrimaryKey(newLeave.getPrimaryKey());

		Assert.assertEquals(existingLeave, newLeave);
	}

	@Test
	public void testFetchByPrimaryKeyMissing() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Leave missingLeave = _persistence.fetchByPrimaryKey(pk);

		Assert.assertNull(missingLeave);
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereAllPrimaryKeysExist()
		throws Exception {
		Leave newLeave1 = addLeave();
		Leave newLeave2 = addLeave();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLeave1.getPrimaryKey());
		primaryKeys.add(newLeave2.getPrimaryKey());

		Map<Serializable, Leave> leaves = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(2, leaves.size());
		Assert.assertEquals(newLeave1, leaves.get(newLeave1.getPrimaryKey()));
		Assert.assertEquals(newLeave2, leaves.get(newLeave2.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereNoPrimaryKeysExist()
		throws Exception {
		long pk1 = RandomTestUtil.nextLong();

		long pk2 = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(pk1);
		primaryKeys.add(pk2);

		Map<Serializable, Leave> leaves = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(leaves.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithMultiplePrimaryKeysWhereSomePrimaryKeysExist()
		throws Exception {
		Leave newLeave = addLeave();

		long pk = RandomTestUtil.nextLong();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLeave.getPrimaryKey());
		primaryKeys.add(pk);

		Map<Serializable, Leave> leaves = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, leaves.size());
		Assert.assertEquals(newLeave, leaves.get(newLeave.getPrimaryKey()));
	}

	@Test
	public void testFetchByPrimaryKeysWithNoPrimaryKeys()
		throws Exception {
		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		Map<Serializable, Leave> leaves = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertTrue(leaves.isEmpty());
	}

	@Test
	public void testFetchByPrimaryKeysWithOnePrimaryKey()
		throws Exception {
		Leave newLeave = addLeave();

		Set<Serializable> primaryKeys = new HashSet<Serializable>();

		primaryKeys.add(newLeave.getPrimaryKey());

		Map<Serializable, Leave> leaves = _persistence.fetchByPrimaryKeys(primaryKeys);

		Assert.assertEquals(1, leaves.size());
		Assert.assertEquals(newLeave, leaves.get(newLeave.getPrimaryKey()));
	}

	@Test
	public void testActionableDynamicQuery() throws Exception {
		final IntegerWrapper count = new IntegerWrapper();

		ActionableDynamicQuery actionableDynamicQuery = LeaveLocalServiceUtil.getActionableDynamicQuery();

		actionableDynamicQuery.setPerformActionMethod(new ActionableDynamicQuery.PerformActionMethod<Leave>() {
				@Override
				public void performAction(Leave leave) {
					Assert.assertNotNull(leave);

					count.increment();
				}
			});

		actionableDynamicQuery.performActions();

		Assert.assertEquals(count.getValue(), _persistence.countAll());
	}

	@Test
	public void testDynamicQueryByPrimaryKeyExisting()
		throws Exception {
		Leave newLeave = addLeave();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Leave.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("leaveId",
				newLeave.getLeaveId()));

		List<Leave> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Leave existingLeave = result.get(0);

		Assert.assertEquals(existingLeave, newLeave);
	}

	@Test
	public void testDynamicQueryByPrimaryKeyMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Leave.class,
				_dynamicQueryClassLoader);

		dynamicQuery.add(RestrictionsFactoryUtil.eq("leaveId",
				RandomTestUtil.nextLong()));

		List<Leave> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testDynamicQueryByProjectionExisting()
		throws Exception {
		Leave newLeave = addLeave();

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Leave.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("leaveId"));

		Object newLeaveId = newLeave.getLeaveId();

		dynamicQuery.add(RestrictionsFactoryUtil.in("leaveId",
				new Object[] { newLeaveId }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(1, result.size());

		Object existingLeaveId = result.get(0);

		Assert.assertEquals(existingLeaveId, newLeaveId);
	}

	@Test
	public void testDynamicQueryByProjectionMissing() throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Leave.class,
				_dynamicQueryClassLoader);

		dynamicQuery.setProjection(ProjectionFactoryUtil.property("leaveId"));

		dynamicQuery.add(RestrictionsFactoryUtil.in("leaveId",
				new Object[] { RandomTestUtil.nextLong() }));

		List<Object> result = _persistence.findWithDynamicQuery(dynamicQuery);

		Assert.assertEquals(0, result.size());
	}

	@Test
	public void testResetOriginalValues() throws Exception {
		Leave newLeave = addLeave();

		_persistence.clearCache();

		Leave existingLeave = _persistence.findByPrimaryKey(newLeave.getPrimaryKey());

		Assert.assertTrue(Objects.equals(existingLeave.getUuid(),
				ReflectionTestUtil.invoke(existingLeave, "getOriginalUuid",
					new Class<?>[0])));
		Assert.assertEquals(Long.valueOf(existingLeave.getGroupId()),
			ReflectionTestUtil.<Long>invoke(existingLeave,
				"getOriginalGroupId", new Class<?>[0]));
	}

	protected Leave addLeave() throws Exception {
		long pk = RandomTestUtil.nextLong();

		Leave leave = _persistence.create(pk);

		leave.setUuid(RandomTestUtil.randomString());

		leave.setGroupId(RandomTestUtil.nextLong());

		leave.setCompanyId(RandomTestUtil.nextLong());

		leave.setUserId(RandomTestUtil.nextLong());

		leave.setUserName(RandomTestUtil.randomString());

		leave.setCreateDate(RandomTestUtil.nextDate());

		leave.setModifiedDate(RandomTestUtil.nextDate());

		leave.setLeaveName(RandomTestUtil.randomString());

		leave.setStartDate(RandomTestUtil.nextDate());

		leave.setEndDate(RandomTestUtil.nextDate());

		leave.setStatus(RandomTestUtil.nextInt());

		leave.setStatusByUserId(RandomTestUtil.nextLong());

		leave.setStatusByUserName(RandomTestUtil.randomString());

		leave.setStatusDate(RandomTestUtil.nextDate());

		_leaves.add(_persistence.update(leave));

		return leave;
	}

	private List<Leave> _leaves = new ArrayList<Leave>();
	private LeavePersistence _persistence;
	private ClassLoader _dynamicQueryClassLoader;
}