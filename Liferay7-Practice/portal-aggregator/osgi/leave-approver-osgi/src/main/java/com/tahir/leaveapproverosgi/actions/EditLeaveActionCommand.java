package com.tahir.leaveapproverosgi.actions;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.tahir.leaveapproverdb.model.Leave;
import com.tahir.leaveapproverdb.service.LeaveLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tana0616 on 7/18/2017.
 */
@Component(
        property = {
                "javax.portlet.name=leaveApprove_portlet",
                "mvc.command.name=leave_editLeave"
        },
        service = MVCActionCommand.class
)
public class EditLeaveActionCommand extends BaseMVCActionCommand {

    private LeaveLocalService leaveService;
    @Reference(unbind = "-")
    protected void setLeaveService(LeaveLocalService leaveService) {
        this.leaveService = leaveService;
    }
    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse)
            throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        String name = ParamUtil.getString(actionRequest, "name");
        Date startDate= ParamUtil.getDate(actionRequest, "startDate",sdf );
        Date endDate = ParamUtil.getDate(actionRequest, "endDate", sdf);

        ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
        long groupId = themeDisplay.getScopeGroupId();

        ServiceContext serviceContext = ServiceContextFactory.getInstance(Leave.class.getName(), actionRequest);
        Leave leave = leaveService.addLeave(serviceContext,name,  startDate, endDate);

       /* Leave leave = leaveService.addLeave(name, themeDisplay.getRealUserId(), groupId,
                themeDisplay.getCompanyId(), startDate, endDate);*/

    }

}
